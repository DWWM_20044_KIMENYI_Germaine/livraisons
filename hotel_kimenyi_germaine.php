<?php
$chambres=[];
$cpt_reserves=0;
$cpt_libres=0;
$login="toto";
$mot_de_passe=1234;

for ($i=1; $i<=20; $i++){
$chambres[$i]=random_int(0,1);
}

for ($i=1; $i<=20; $i++){
    if($chambres[$i]==1){
    $reserves[$cpt_reserve]=$i;  
    $cpt_reserves++;
    }
}
   
for ($i=1; $i<=20; $i++){
    if ($chambres[$i]==0){
    $libres[$cpt_libres]=$i;
    $cpt_libres++;
    }
}

do {
   
    // Affichage du menu

    echo "------------------------------- MENU HOTEL DWWM PHP -----------------------------------------".PHP_EOL;
    echo "A - Afficher l’état de l’hôtel".PHP_EOL;
    echo "B - Afficher le nombre de chambres réservées".PHP_EOL;
    echo "C - Afficher le nombre de chambres libres".PHP_EOL;
    echo "D - Afficher le numéro de la première chambre vide".PHP_EOL;
    echo "E - Afficher le numéro de la dernière chambre vide".PHP_EOL;
    echo "F - Réserver une chambre".PHP_EOL;
    echo "G - Libérer une chambre".PHP_EOL;
    echo PHP_EOL;
    echo "Q - Quitter ".PHP_EOL;
    echo PHP_EOL;
    echo "----------------------------------------------------------------------------------------------".PHP_EOL;

    $choix=readline("Votre choix : ");
    $choix=strtoupper($choix);

    // A - Afficher l’état de l’hôtel
    if ($choix=="A"){
    echo PHP_EOL;
    echo ("L'Hôtel dispose de:".PHP_EOL);
    echo ($cpt_libres." chambre(s) libre(s)".PHP_EOL);
    echo ($cpt_reserves." chambre(s) réservée(s)".PHP_EOL);
    }

    // B - Afficher le nombre de chambres réservées
    elseif ($choix=="B"){
        echo PHP_EOL;
        echo $cpt_reserves." chambre(s) réservée(s)".PHP_EOL;
    }


    // C - Afficher le nombre de chambres libres
    elseif ($choix=="C"){
        echo PHP_EOL;
        echo $cpt_libres." chambre(s) libre(s)".PHP_EOL;
    }

    // D - Afficher le numéro de la première chambre vide
    elseif ($choix=="D"){
        for ($i=1; $i<=20; $i++){
            if($chambres[$i]==0){
                echo PHP_EOL;
                echo ("La première chambre vide est la N°:".$i);
                break;
            }
        }
    }

    // E - Afficher le numéro de la dernière chambre vide

    elseif ($choix=="E"){
        for ($i=20; $i>=1; $i--){
            if($chambres[$i]==0){
            echo PHP_EOL;
            echo ("La dernière chambre vide est la N°:".$i);
            break;
            }
        }

    }

    // F - Réserver une chambre 
        // login et mot de passe stockés dans des variables 

        elseif ($choix=="F"){
            $saisie_login=readline("veuillez saisir votre identifiant: ");
            $saisie_mot_de_passe=readline("veuillez saisir votre mot de passe: ");
            if ($login==$saisie_login && $mot_de_passe==$saisie_mot_de_passe){
                for ($i=1; $i<=20; $i++){
                    if($chambres[$i]==0){
                        $chambres[$i]=1; 
                        echo PHP_EOL;
                        echo ("Vous venez de réserver la chambre N°:".$i);
                    break;
                    }
                }
            }  
            else { 
                echo PHP_EOL;
                echo ("Votre identifiant et/ou mot de passe sont incorrects");
                echo PHP_EOL;
            }   
        }

    // G - Libérer une chambre
        // login et mot de passe stockés dans des variables 

        elseif ($choix=="G"){
            $saisie_login=readline("veuillez saisir votre identifiant: ");
            $saisie_mot_de_passe=readline("veuillez saisir votre mot de passe: ");
            if ($login==$saisie_login && $mot_de_passe==$saisie_mot_de_passe){
                for ($i=20; $i>=0; $i--){
                    if($chambres[$i]==1){
                        $chambres[$i]=0; 
                        echo PHP_EOL;
                        echo ("Vous venez de libérer la chambre N°:".$i);
                        echo PHP_EOL;
                        break;
                    }
                }      
            }
            else { 
                echo PHP_EOL;
                echo ("Votre identifiant et/ou mot de passe sont incorrects");
                echo PHP_EOL;
            } 
        }

    // Q - Quitter
    elseif ($choix=="Q"){
        echo PHP_EOL;
        exit ("À bientôt!".PHP_EOL);
    }

    else {
        echo PHP_EOL;
        echo "Votre choix est incorrect!";
        echo PHP_EOL;
    }
    echo PHP_EOL;
    $choix_continuer=readline("Voulez-vous continuer? OUI/NON: ");
    $choix_continuer=strtoupper($choix_continuer);
} while ($choix!="Q" && $choix_continuer!="NON");

?>